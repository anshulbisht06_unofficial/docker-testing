FROM node:alpine

WORKDIR '/docker-testing'

COPY package.json .
RUN npm install
COPY . .

CMD ["npm","start"]